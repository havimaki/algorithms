import * as THREE from 'three';

function setScene(that) {
  that.scene = new THREE.Scene()
}

function setCamera(that, fov, near, far) {
  that.scene = new THREE.Scene()
  that.camera = new THREE.PerspectiveCamera( fov, window.innerWidth/window.innerHeight, near, far );
  that.camera.position.z = 10;
}

function setRenderer(that, color) {
  that.renderer = new THREE.WebGLRenderer({ antialias: true })
  that.renderer.setClearColor(color);
  that.renderer.setSize(400, 400);
  that.mount.appendChild(that.renderer.domElement);
}

function setShapes(that, array) {
  let geometry, material, line, i, group;
  i = 0;
  group = new THREE.Group();
  // for each item in the array, set a geo length and add to scene
  for(i === 0; i < array.length; i++) {
    geometry = new THREE.BoxGeometry( 0.1, array[i], 1 );
    material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
    line = new THREE.Mesh( geometry, material );
    line.position.set( i, 0, 0);
    group.add( line );
  }
  that.scene.add( group );
}

function displayScene(that) {
  that.renderer.render(that.scene, that.camera);
}

function startAnimation(that) {
  if (!that.frameId) {
    that.frameId = requestAnimationFrame(that.animate);
  }
}

function stopAnimation(that) {
  cancelAnimationFrame(that.frameId);
}

function animate(that) {
  // this.line.rotation.x += 0.01
  // this.line.rotation.y += 0.01
  that.renderScene();
  that.frameId = window.requestAnimationFrame(that.animate);
}

export { 
  setScene, 
  setCamera, 
  setRenderer, 
  setShapes,
  displayScene, 
  animate, 
  startAnimation, 
  stopAnimation
} 