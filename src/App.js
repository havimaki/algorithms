import React, { Component } from 'react';
import BubbleSort from './components/sorting/bubbleSort.js';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">          
          <p>Algorithms.</p>
          <BubbleSort />
        </header>
      </div>
    );
  }
}

export default App;
