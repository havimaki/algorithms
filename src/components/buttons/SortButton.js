import React from 'react';
import { Button } from 'antd';


class SortButton extends React.Component {
  render() {
    return (
      <Button type="primary" onClick={() => this.handleSort()}>Sort Shapes</Button>
    )
  }
}

export default SortButton;