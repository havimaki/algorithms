import React from 'react';
import { Form, Button, InputNumber } from 'antd';
const FormItem = Form.Item;

class BubbleSortArray extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let lineData = [];
        Object.keys(values).forEach(function (number) {
          lineData.push(values[number])
        });
        this.props.handleLineInsert(lineData) 
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <React.Fragment>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <FormItem>
            {getFieldDecorator('number_1', {})(
              <InputNumber placeholder="number 1" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('number_2', {})(
              <InputNumber placeholder="number 2" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('number_3', {})(
              <InputNumber placeholder="number 3" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('number_4', {})(
              <InputNumber placeholder="number 4" />
            )}
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit" className="login-form-button">Submit</Button>
          </FormItem>
        </Form>
      </React.Fragment>
    )
  }
}

const BubbleSortForm = Form.create({})(BubbleSortArray);
export default BubbleSortForm;