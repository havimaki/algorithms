import React from 'react';
import * as THREEJS from '../../utils/three.js';
import BubbleSortForm from '../forms/BubbleSortForm.js';
import SortButton from '../buttons/SortButton.js';

class BubbleSort extends React.Component {

  componentDidMount() {
    THREEJS.setScene(this) 
    THREEJS.setCamera(this, 75, 0.1, 1000)
    THREEJS.setRenderer(this, '#000000')
    THREEJS.displayScene(this);
  }

  updateShapes = (values) => {
    THREEJS.setShapes(this, values);
    THREEJS.displayScene(this);
  }

  sortShapes(){
    let lines = this.state.lines;
    let swapped;
     do {
      // set swapped to false
      // if the items are swapped, set swapped to true
      // once items are no longer swapped, it stays false and exits the while loop
      swapped = false;

      let i = 0;
      for(i === 0; i < lines.length; i++) {
        // if `this` item is larger than `this + 1`
        // 1. let `temporary` be `this` 
        // 2. let `this` be `this + 1`
        // 3. let `this + 1` be `temporary`
        if(lines[i] > lines[i+1]) {
          let temporary = lines[i];
          lines[i] = lines[i+1];
          lines[i+1] = temporary;
          swapped = true
        }
      }
    } while (swapped)
    return lines;
  }

  render() {
    return (
      <React.Fragment>
        <BubbleSortForm 
          handleLineInsert={this.updateShapes}/>
        <SortButton />
        <div 
          style={{ width: '400px', height: '400px' }} 
          ref={(mount) => { this.mount = mount }}>
        </div>
      </React.Fragment>
    )
  }
}

export default BubbleSort;