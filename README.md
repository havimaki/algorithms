Watch me do cool things with algorithms.
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

--

## Extra info
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).<br>
`npm start`Open [http://localhost:3000](http://localhost:3000) to view it in the browser.<br>
`npm test` See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.<br>
`npm run build` Builds the app for production to the `build` folder.<br>
See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

